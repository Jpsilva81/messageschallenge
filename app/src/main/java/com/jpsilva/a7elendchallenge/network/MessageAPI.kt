package com.jpsilva.a7elendchallenge.network

import com.jpsilva.a7elendchallenge.model.DataModel
import com.jpsilva.a7elendchallenge.model.Messages
import com.jpsilva.a7elendchallenge.model.Users
import io.reactivex.Observable
import retrofit2.http.GET

/**
 * The interface which provides methods to get result of webservices
 */
interface MessageAPI {
    /**
     * Get the list of the pots from the API
     */
    @GET("/conversation")
    fun getMessages(): Observable<DataModel>
}