package com.jpsilva.a7elendchallenge.base

import android.arch.lifecycle.ViewModel
import com.jpsilva.a7elendchallenge.network.NetworkModule
import com.jpsilva.a7elendchallenge.util.DaggerViewModelInjector
import com.jpsilva.a7elendchallenge.viewmodel.MessagesListViewModel
import com.jpsilva.a7elendchallenge.util.ViewModelInjector

abstract class BaseViewModel:ViewModel(){
    private val injector: ViewModelInjector = DaggerViewModelInjector
            .builder()
            .networkModule(NetworkModule)
            .build()

    init {
        inject()
    }

    /**
     * Injects the required dependencies
     */
    private fun inject() {
        when (this) {
            is MessagesListViewModel -> injector.inject(this)
        }
    }
}