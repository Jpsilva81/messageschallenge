package com.jpsilva.a7elendchallenge.model

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import com.squareup.moshi.Json

@Entity
data class Users(@Json(name = "avatarId")
                 val avatarId: String,
                 @Json(name = "name")
                 val name: String,
                 @Json(name = "id")
                 @field:PrimaryKey
                 val id: Int)