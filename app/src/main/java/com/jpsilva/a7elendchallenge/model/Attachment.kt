package com.jpsilva.a7elendchallenge.model

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import com.squareup.moshi.Json

@Entity
data class Attachment(@Json(name = "id")
                      @field:PrimaryKey
                      var id: String,
                           @Json(name = "title")
                           var title: String,
                           @Json(name = "url")
                           var url: String,
                           @Json(name = "thumbnailUrl")
                           var thumbnailUrl: String)