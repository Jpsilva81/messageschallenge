package com.jpsilva.a7elendchallenge.model

import com.squareup.moshi.Json


data class DataModel(@Json(name = "messages")
                 var messages : List<Messages>,
                 @Json(name = "users")
                 var users: List<Users>)