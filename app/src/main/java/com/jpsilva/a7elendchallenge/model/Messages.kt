package com.jpsilva.a7elendchallenge.model

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.support.annotation.NonNull
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
@Entity
data class Messages( @Json(name = "id")
                     @field:PrimaryKey
                     var id: Int,
                     @Json(name = "userId")
                     var userId: Int,
                     @Json(name = "content")
                     var content: String,
                     @Json(name = "attachments")
                     var attachments: List<Attachment>?)

