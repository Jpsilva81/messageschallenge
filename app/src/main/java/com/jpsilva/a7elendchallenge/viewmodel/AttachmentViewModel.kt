package com.jpsilva.a7elendchallenge.viewmodel

import android.arch.lifecycle.MutableLiveData
import com.jpsilva.a7elendchallenge.base.BaseViewModel
import com.jpsilva.a7elendchallenge.model.Attachment


class AttachmentViewModel: BaseViewModel() {
    private val url = MutableLiveData<String>()
    private val text = MutableLiveData<String>()
    fun bind(attach: Attachment){
        url.value = attach.thumbnailUrl
        text.value = attach.title
    }

    fun getImageUrl(): MutableLiveData<String> {
        return url
    }

    fun getTitle(): MutableLiveData<String> {
        return text
    }
}