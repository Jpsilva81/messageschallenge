package com.jpsilva.a7elendchallenge.viewmodel

import android.arch.lifecycle.MutableLiveData

import com.jpsilva.a7elendchallenge.base.BaseViewModel
import com.jpsilva.a7elendchallenge.model.Messages
import com.jpsilva.a7elendchallenge.model.Users
import com.jpsilva.a7elendchallenge.ui.adapter.AttachmentAdapter

class MessageViewModel: BaseViewModel() {
    private val content = MutableLiveData<String>()
    private val user_name = MutableLiveData<String>()
    private val image_url = MutableLiveData<String>()
    val attachmentAdapter: AttachmentAdapter = AttachmentAdapter()

    fun bind(message: Messages, user : Users){
        content.value = message.content
        user_name.value = user.name
        image_url.value = user.avatarId
        if (message.attachments != null && message.attachments!!.isNotEmpty())
            attachmentAdapter.updateAttachmentList(message.attachments!!)
    }

    fun getContent():MutableLiveData<String>{
        return content
    }
    fun getUserName():MutableLiveData<String>{
        return user_name
    }
    fun getImageUrl():MutableLiveData<String>{
        return image_url
    }
    fun getAdapter() : AttachmentAdapter {
        return attachmentAdapter
    }
}