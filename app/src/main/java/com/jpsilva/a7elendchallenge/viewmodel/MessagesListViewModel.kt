package com.jpsilva.a7elendchallenge.viewmodel

import android.arch.lifecycle.MutableLiveData
import android.view.View
import com.jpsilva.a7elendchallenge.R
import com.jpsilva.a7elendchallenge.base.BaseViewModel
import com.jpsilva.a7elendchallenge.database.MessagesDao
import com.jpsilva.a7elendchallenge.database.UsersDao
import com.jpsilva.a7elendchallenge.model.DataModel
import com.jpsilva.a7elendchallenge.model.Messages
import com.jpsilva.a7elendchallenge.model.Users

import com.jpsilva.a7elendchallenge.network.MessageAPI
import com.jpsilva.a7elendchallenge.ui.adapter.MessageAdapter
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import javax.inject.Inject

class MessagesListViewModel(private val messagesDao: MessagesDao,
                            private val usersDao: UsersDao) : BaseViewModel() {
    @Inject
    lateinit var messagesApi: MessageAPI

    private lateinit var subscription: Disposable
    val loadingVisibility: MutableLiveData<Int> = MutableLiveData()
    val errorMessage: MutableLiveData<Int> = MutableLiveData()
    val errorClickListener = View.OnClickListener { loadMessages() }
    val messageAdapter: MessageAdapter = MessageAdapter()

    init {
        loadMessages()
    }

    private fun loadMessages() {
        subscription = Observable.fromCallable { messagesDao.all;usersDao.all }
                .concatMap { dbMessagesList ->
                    if (dbMessagesList.isEmpty())
                        messagesApi.getMessages().concatMap { apiMessagesList ->
                            messagesDao.insertAll(*apiMessagesList.messages.toTypedArray())
                            Observable.just(apiMessagesList)
                        }
                    else
                        Observable.just(dbMessagesList)
                }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { onRetrieveMessageListStart() }
                .doOnTerminate { onRetrieveMessageListFinish() }
                .subscribe(
                        // Add result
                        { result ->
                            // api call
                            if (result is DataModel)
                                onRetrieveMessageListSuccess(result.messages, result.users)
                            // database
                            else {
                                doAsync {
                                    val messages = messagesDao.all
                                    val users = usersDao.all
                                    uiThread {
                                        onRetrieveMessageListSuccess(messages, users)
                                    }
                                }
                            }
                        },
                        { onRetrieveMessageListError() }
                )
    }

    override fun onCleared() {
        super.onCleared()
        subscription.dispose()
    }

    private fun onRetrieveMessageListStart() {
        loadingVisibility.value = View.VISIBLE
        errorMessage.value = null
    }

    private fun onRetrieveMessageListFinish() {
        loadingVisibility.value = View.GONE
    }

    private fun onRetrieveMessageListSuccess(messageList: List<Messages>, users: List<Users>) {
        messageAdapter.updateMessageList(messageList, users)
    }

    private fun onRetrieveMessageListError() {
        errorMessage.value = R.string.post_error

    }
}