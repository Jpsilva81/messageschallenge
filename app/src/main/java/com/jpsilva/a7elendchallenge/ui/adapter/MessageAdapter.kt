package com.jpsilva.a7elendchallenge.ui.adapter

import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.jpsilva.a7elendchallenge.BR
import com.jpsilva.a7elendchallenge.R
import com.jpsilva.a7elendchallenge.databinding.ItemMessageReceivedBinding
import com.jpsilva.a7elendchallenge.databinding.ItemMessageSentBinding
import com.jpsilva.a7elendchallenge.model.Messages
import com.jpsilva.a7elendchallenge.model.Users
import com.jpsilva.a7elendchallenge.viewmodel.MessageViewModel

class MessageAdapter: RecyclerView.Adapter<MessageAdapter.ViewHolder>() {
    private lateinit var messageList:List<Messages>
    private lateinit var usersList:List<Users>

    override fun getItemViewType(position: Int): Int {
        return messageList[position].userId
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder.create(parent, viewType)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindTo(messageList[position], usersList[messageList[position].userId-1])
    }

    override fun getItemCount(): Int {
        return if(::messageList.isInitialized) messageList.size else 0
    }

    fun updateMessageList(messageList:List<Messages>, usersList:List<Users> ){
        this.messageList = messageList
        this.usersList = usersList
        notifyDataSetChanged()
    }

    class ViewHolder(private val binding: ViewDataBinding):RecyclerView.ViewHolder(binding.root){

        private val viewModel = MessageViewModel()

        fun bindTo(message: Messages, user : Users) {
            viewModel.bind(message, user)
            binding.setVariable(BR.viewModel, viewModel)
            binding.executePendingBindings()
        }

        companion object {

            const val MYSELF = 1
            fun create(parent: ViewGroup, viewType: Int): ViewHolder {
                return when (viewType) {
                    MYSELF -> {
                        val binding : ItemMessageSentBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.item_message_sent, parent, false)
                        ViewHolder(binding)
                    }
                    else -> {
                        val binding : ItemMessageReceivedBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.item_message_received, parent, false)
                        ViewHolder(binding)
                    }
                }
            }
        }
    }
}