package com.jpsilva.a7elendchallenge.ui.adapter

import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.jpsilva.a7elendchallenge.R
import com.jpsilva.a7elendchallenge.databinding.ItemAttachmentBinding
import com.jpsilva.a7elendchallenge.model.Attachment
import com.jpsilva.a7elendchallenge.viewmodel.AttachmentViewModel


class AttachmentAdapter: RecyclerView.Adapter<AttachmentAdapter.ViewHolder>() {

    private var attachmentList:List<Attachment> = emptyList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: ItemAttachmentBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.item_attachment, parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(attachmentList[position])
    }
    override fun getItemCount(): Int {
        return if (attachmentList.isEmpty()) 0 else attachmentList.size
    }
    fun updateAttachmentList(attachmentList:List<Attachment> ){
        this.attachmentList = attachmentList
        notifyDataSetChanged()
    }

    class ViewHolder(private val binding: ItemAttachmentBinding): RecyclerView.ViewHolder(binding.root){

        private val viewModel = AttachmentViewModel()

        fun bind(attachment:Attachment){
            viewModel.bind(attachment)
            binding.viewModel = viewModel
        }
    }

}