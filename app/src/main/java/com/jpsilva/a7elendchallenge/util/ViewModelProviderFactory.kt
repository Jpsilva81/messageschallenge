package com.jpsilva.a7elendchallenge.util

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import android.arch.persistence.room.Room
import android.support.v7.app.AppCompatActivity
import com.jpsilva.a7elendchallenge.database.AppDatabase
import com.jpsilva.a7elendchallenge.viewmodel.MessagesListViewModel

class ViewModelProviderFactory(private val activity: AppCompatActivity): ViewModelProvider.Factory{
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(MessagesListViewModel::class.java)) {
            val db = Room.databaseBuilder(activity.applicationContext, AppDatabase::class.java, "msg").build()
            @Suppress("UNCHECKED_CAST")
            return MessagesListViewModel(db.messagesDao(), db.usersDao()) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}