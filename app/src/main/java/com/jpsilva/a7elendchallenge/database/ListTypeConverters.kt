package com.jpsilva.a7elendchallenge.database

import android.arch.persistence.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.jpsilva.a7elendchallenge.model.Attachment
import java.util.*

class ListTypeConverters {

    var gson = Gson()

        @TypeConverter
        fun stringToList(data: String?): List<Attachment>? {
            if (data == "null") {
                return Collections.emptyList()
            }
            val listType = object : TypeToken<List<Attachment>>() {
            }.type
            return gson.fromJson(data, listType)
        }

        @TypeConverter
        fun listToString(list: List<Attachment>?): String {
            return gson.toJson(list)
        }
}