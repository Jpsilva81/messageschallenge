package com.jpsilva.a7elendchallenge.database

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import com.jpsilva.a7elendchallenge.model.Users

@Dao
interface UsersDao {
    @get:Query("SELECT * FROM Users")
    val all: List<Users>

    @Insert
    fun insertAll(vararg user: Users)
}