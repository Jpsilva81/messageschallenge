package com.jpsilva.a7elendchallenge.database

import android.arch.persistence.room.*
import com.jpsilva.a7elendchallenge.model.Messages

@Dao
interface MessagesDao {
    @get:Query("SELECT * FROM Messages")
    val all: List<Messages>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(vararg message: Messages)
}