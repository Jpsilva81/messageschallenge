package com.jpsilva.a7elendchallenge.database

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.TypeConverters
import com.jpsilva.a7elendchallenge.model.Attachment
import com.jpsilva.a7elendchallenge.model.Messages
import com.jpsilva.a7elendchallenge.model.Users

@Database(entities = arrayOf(Messages::class, Users::class,Attachment::class), version = 1)
@TypeConverters(ListTypeConverters::class)
abstract class AppDatabase : RoomDatabase() {
    abstract fun messagesDao(): MessagesDao
    abstract fun usersDao(): UsersDao
}